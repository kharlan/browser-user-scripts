# Browser scripts to work with WMF platforms

These scripts are designed to make it easier to work with various Wikimedia platforms, such as Phabricator or Gerrit.
They can be used with browser script managers such as [Tampermonkey](https://tampermonkey.net/) or [Greasemonkey](https://www.greasespot.net/).

## Prerequisites

Install the Tampermonkey extension for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/) or [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).

## PhabLink
Adds buttons to Phabricator that copy a formatted title and link combination for different markup languages:
* Markdown (e.g. to be used in Mattermost or Phabricator)
* Wikitext, external link (to be used in MediaWiki)
* Wikitext, but T-ID-only and as an internal interwiki link (to be used in MediaWiki)
* HTML (e.g. to be used in Google Docs or GMail)

[Install PhabLink user script](https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/phabButtons.user.js)

![Screenshot of Buttons on a Phabricator task](docs/images/phabButtons.png)
## GerritLink
Adds a markdown button to Gerrit.

[Install GerritLink user script](https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/gerritButtons.user.js)
