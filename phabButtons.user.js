// ==UserScript==
// @name     Phabricator copy link button
// @description  Creates a number of Phabricator link copy buttons
// @author   @micgro42, @lilients
// @namespace https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/
// @version  1.1.1
// @downloadURL https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/phabButtons.user.js
// @updateURL   https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/phabButtons.user.js
// @license  MIT
// @grant    none
// @match    https://phabricator.wikimedia.org/T*
// ==/UserScript==

// Remove the ⚓ and space which just adds visual noise
const title = document.title.replace('⚓ ', '');
const url = location.origin + location.pathname;
const phabId = location.pathname.split('/')[1];

addButtonToDom(' M⬇️', 'Markdown', getPlainTextClickHandler(`[${title}](${url})`) )
addButtonToDom(' ', 'Wikitext', getPlainTextClickHandler(`[${url} ${title}]`) )
addButtonToDom('  iw', 'Wikitext (interwiki link)', getPlainTextClickHandler(`[[phab:${phabId}|${phabId}]]`) )
addButtonToDom(' HTML', 'HTML', htmlClickHandler)


function addButtonToDom(buttonLabel, formattingType, clickHandler) {
  const button = document.createElement('button');
  button.style['font-family'] = 'FontAwesome';
  button.appendChild(document.createTextNode(buttonLabel));
  button.title = `Copy link with title formatted in ${formattingType}`;
  button.addEventListener('click', clickHandler);
  getButtonContainer().appendChild(button);
}

function getButtonContainer() {
    let container = document.getElementById('copyLinkButtonContainer');
    if (!container) {
        container = document.createElement('div');
        container.id = 'copyLinkButtonContainer';
        container.style.display = 'inline-flex';
        container.style['align-items'] = 'stretch';
        document.querySelector('.phui-crumbs-actions').prepend(container);
    }
    return container;
}

function getPlainTextClickHandler ( syntax ) {
    return () => {
        navigator.clipboard.writeText(syntax).then(() => {
            console.log('copied!');
        });
    };
}

function htmlClickHandler () {
  const linkHTML = `<a href=${url}>${title}</a>`;

  if ( typeof ClipboardItem === 'undefined' ) {
    console.log('trying workaround for Firefox without ClipboardItem');

    copyAsHTML(linkHTML);
  } else {

    console.log('using new ClipboardItem API');

    const clipboardItem = new
            ClipboardItem({
            'text/html':  new Blob([linkHTML], {type: 'text/html'}),
            'text/plain': new Blob([linkHTML], {type: 'text/plain'}),
            });

    navigator.clipboard.write([clipboardItem]).then(() => {
        console.log('html copied!');
    }, (error) => console.error(error));
  }
}

// adapted from https://stackoverflow.com/a/34192073/3293343
function copyAsHTML (html) {

    var container = document.createElement('div')
    container.innerHTML = html.trim();

    container.style.position = 'fixed'
    container.style.pointerEvents = 'none'
    container.style.opacity = 0

    document.body.appendChild(container)

    window.getSelection().removeAllRanges()

    var range = document.createRange()
    range.selectNode(container)
    window.getSelection().addRange(range)

    document.execCommand('copy')

    document.body.removeChild(container)
}
