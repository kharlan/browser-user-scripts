// ==UserScript==
// @name     Gerrit Change copy link button
// @description  Creates buttons to copy links to Gerrit changes
// @author   @micgro42
// @namespace  https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/
// @version  1
// @downloadURL https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/gerritButtons.user.js
// @updateURL   https://gitlab.wikimedia.org/repos/wmde/browser-user-scripts/-/raw/main/gerritButtons.user.js
// @grant    none
// @match    https://gerrit.wikimedia.org/*
// ==/UserScript==

let currentLocation = document.location.href;
let markdownButton;

const observer = new MutationObserver((mutationList) => {
  if (currentLocation !== document.location.href) {
    // location changed!
    currentLocation = document.location.href;

    console.log('location changed');
    
    if ( markdownButton ) {
      markdownButton.remove();
      markdownButton = null;
    }
    
    maybeAddCopyLinkButton();
  }
});

observer.observe(
  document.body,
  {
    childList: true,

    // important for performance
    subtree: false
  }
);



function addCopyLinkButton() {
  const url = window.location.toString();
  const title = document.title.slice(0, -1 * ' · Gerrit Code Review'.length);
  const markdownLinkText = `[${title}](${url})`;
  markdownButton = document.createElement('button');
  markdownButton.appendChild(document.createTextNode('Copy markdown'));
  markdownButton.addEventListener('click', () => {
    console.log('clicked!');
    navigator.clipboard.writeText(markdownLinkText).then(() => {
      console.log('copied!');
    });
  });
  markdownButton.style.position = 'fixed';
  markdownButton.style.bottom = 0;
  markdownButton.style.left = '45%';
  markdownButton.style['z-index'] = 999999999;
  document.body.appendChild(markdownButton);
}

function maybeAddCopyLinkButton(){
  if (currentLocation.slice(0, 'https://gerrit.wikimedia.org/r/c/'.length) !== 'https://gerrit.wikimedia.org/r/c/') {
    console.log('not a gerrit change, current location is: ' + currentLocation);
    return;
	}
  console.log('Gerrit change, trying to add button');
  setTimeout(() => {
    if ( document.title ) {
      console.log('title available 1st try! 🙌 ' + document.title);
      addCopyLinkButton();
    } else {
      console.log('title missing, trying again 🤔');
      setTimeout(() => {
        if ( document.title ) {
          console.log('title available 2nd try! 👍 ' + document.title);
          addCopyLinkButton();
        } else {
          console.log('title still missing 😭');
        }  
      }, 3000);
    }  
  }, 3000);
}
maybeAddCopyLinkButton();